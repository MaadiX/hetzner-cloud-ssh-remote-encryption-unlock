This steps will allow you to full encrypt a VPS on Hetzner, and unlock it at 
boot time via ssh.   
Based on different tutorials which do not work with Hetzner because of network 
configuration.  

https://init6.me/unlock-vps-full-disk-encryption/  
https://github.com/TheReal1604/disk-encryption-hetzner/blob/master/debian/debian_swraid_lvm_luks.md


This tutorial  includes how to enable dropbear ssh access on boot to unlock the 
encrypted disk and how to encrypt existing disk.  
It is for a server with 2 partictions:  

```
/dev/sda /boot
/dev/sda2 /
```

Change following commands according to the name of your devices. 
The network interface is eth0. If your one differs, change commands using your
device, or update your device name to old eth0  

## Change device name to eth0 (optional)

in  /etc/default/grub add: 


```
GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"
```

in  /etc/networkInterfaces

```
# FOR STATIC IP ADDRESS / HETZNER CLOUD vps NEED 
auto eth0
iface eth0 inet static
        address YOUR IP
        netmask 255.255.255.255
        gateway 172.31.1.1
        pointopoint 172.31.1.1
        dns-nameservers 213.133.98.98 213.133.99.99 213.133.100.10
```


# Install dropbear and busybox  

```
apt-get install busybox dropbear
```


Copy your ssh pubblic Key in /etc/dropbear-initramfs/authorized_keys
NOTE: Many tutorials say it must be placed in 
/etc/initramfs-tools/root/.ssh/authorized_keys but this didn't work for us.


## Activate and configure dropbear/busybox

```
sed -i "s/NO_START=1/NO_START=0/" /etc/default/dropbear
sed -i "s/^#CRYPTSETUP=$/CRYPTSETUP=y/" /etc/cryptsetup-initramfs/conf-hook
//use port 2000 fro dropbear...you may wish to use a different one
sed -i "s/^#DROPBEAR_OPTIONS=$/DROPBEAR_OPTIONS=\"-p 2000 -s -j -k -I 60\"/" /etc/dropbear-initramfs/config
sed -i "s/BUSYBOX=auto/BUSYBOX=y/" /etc/initramfs-tools/initramfs.conf
```


Now we need to disable eth0 after we are done with dropbear so the system can 
bring it back online using system information.


```
    sudo vi /usr/share/initramfs-tools/scripts/init-bottom/dropbear
    #Add the following at the bottom of the script:
    ifconfig eth0 down
```
 
## Disable dropbear on boot so OpenSSH can be used.

```
    sudo update-rc.d -f dropbear remove
```

## Network configuration  

We need to enable network configuration in intiramfs, so we can access the 
server via ssh on initramfs with dropbear. 
Normally, adding the line IP=SERVER:IP::GATEWAY:NETMASK::DEVICE:off 
in /etc/initramfs-tools/initramfs.conf would work,
but not in HETZNER

So, for servers who just need IP, Netmask and gateway you can add following 
line in /etc/initramfs-tools/initramfs.conf, just after 
the DEVICE= 

EJ:
```
DEVICE= #Leave it empty  
IP=11.111.11.214::11.111.11.222:255.255.255.240::eth0:off
```

where
IP=SERVER:IP::GATEWAY:NETMASK::DEVICE:off

For Hetzner Network do not add the IP param but edit

 /usr/share/initramfs-tools/scripts/init-premount/dropbear

and just below 
[ "$boot" != nfs ] || configure_networking

add following lines

```

    ip link set eth0 up
    ip address add dev eth0 YOUR:IP peer 172.31.1.1
    ip ro add default via 172.31.1.1
    
    
```

Then update intiramfs:  

```
sudo update-initramfs -u
```

If you don't have any RIDE, you may get this error: 
The error says W: mdadm: /etc/mdadm/mdadm.conf defines no arrays.

To solve it, add  /etc/mdadm/mdadm.conf
```
ARRAY <ignore> devices=/dev/sda
```


## ENCRYPT DISK (Only if you don't already have encrypted volume) 

Our encrypted volume is /dev/sda2 mapped to /dev/mapper/cryptroot 
Skip following steps if you already have an ecrypted volume and jump 
to  [#update-crypttab](#update-crypttab) section. 

Reboot in Rescue mode 

### Backups root partition 

```
mkdir /oldroot/
mount /dev/sda2 /mnt
rsync -a /mnt/ /oldroot/
umount /mnt
```
### Encrypt partition

```
cryptsetup --cipher aes-xts-plain64 --key-size 256 --hash sha256 --iter-time 6000 luksFormat /dev/sda2
```

### decrypt  

```
cryptsetup luksOpen /dev/sda2 cryptroot
pvcreate /dev/mapper/cryptroot
mkfs.ext4 /dev/mapper/cryptroot
mount /dev/mapper/cryptroot /mnt/
```

### restore Backup

```
rsync -a /oldroot/ /mnt/
```
### Enter chroot to complete configuration  

```
mount --bind /dev /mnt/dev
mount --bind /sys /mnt/sys
mount --bind /proc /mnt/proc
mount /dev/sda1 /mnt/boot
chroot /mnt

vi /etc/fstab - Use new /dev/mapper/cryptroot device instead of original /dev/sda2

/dev/mapper/cryptroot	/ ext4 defaults,discard 0 0
```

### Update crypttab 
Run following command even if you already had ancrypted volume. 


In /etc/crypttab add following line

```
cryptroot /dev/sda2 none luks
```

NOTE: Depending on your partitions, you may need to customize your crypttab configuration. 
Some users have experienced problems: 
(Error: Timeout reached while waiting for askpass.) 
Which was solved adding each separate logical volume into /etc/crypttab 
instead of the volume group as a whole. 

See: https://serverfault.com/questions/907254/cryproot-unlock-with-dropbear-timeout-while-waiting-for-askpass#912460

Now run
```
update-initramfs -u
update-grub
grub-install /dev/sda
```

If you were in chroot environmet (for encrypting the volume) exit and umount
```
    exit
    umount /mnt/boot /mnt/proc /mnt/sys /mnt/dev
    umount /mnt
    sync

```
 Now reboot the system.
 
Acees server with ssh in port 2000 (for dropbear)  
ssh -p 2000 -í /path/to/key root@IP.VM

If everything was fine you should see the busybox shell

type
```
cryptroot-unlock
```

and insert the password to unlock the encrypted device.  
Once descripted , the coneection is interrupted and you should be able to access the server as usual

 
#TODO:  

Add script in /etc/initramfs-tools/hooks/ to avoid having to type  cryptroot-unlock  
Chek optimal configuartion for scurity  


